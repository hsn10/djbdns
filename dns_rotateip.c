#include "byte.h"

void dns_rotateip(char *s, unsigned int n)
{
  unsigned int i;
  char tmp[4];

  i = n >> 2;
  while (i > 1) {
    --i;
    byte_copy(tmp,4,s + (i << 2));
    byte_copy(s + (i << 2),4,s);
    byte_copy(s,4,tmp);
  }
}
